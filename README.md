# Sheep for Java OOP

Sheep for Java OOP is an implementation sheep/woolmark for Java with Object Oriented Programming.

# Design and Architecture

## Package, Class and Interface

Package, Class and Interface architecture is designed based on Apache HTTP Core Java Library.

`org.bitbucket.woolmark` package has only the interfaces.
`org.bitbucket.woolmark.impl` package has the abstract classes and basic classes.
`org.bitbucket.woolmark.impl.awt` and `org.bitbucket.woolmark.impl.swing` packages 
 have the implemented classes depends on AWT and Swing.

## Event Driven Architecture

This application is round robin model architecture.
All of object #calc and #draw methods are called by system.

## Real World Object Mapping

Any real world objects are mapped into the classes.
One real world object is mapped into one java object.
The object has UI and Function, and works only by myself.

That means this application is not MVC model. There is not controller, view and model.

# License

WTFPL
