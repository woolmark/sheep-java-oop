package org.bitbucket.woolmark;

public interface Object {

    int getX();

    int getY();

    int getWidth();

    int getHeight();
    
    void setX(final int x);
    
    void setY(final int y);

    void move(final int x, final int y);

    void setPosition(final int x, final int y);

    void setSize(final int width, final int height);
    
    void setWidth(final int width);
    
    void setHeight(final int height);
    
    void ticktack();

    void draw(final Drawer drawer);

    boolean isAlive();

    void alive(final boolean alive);

    World getWorld();

}
