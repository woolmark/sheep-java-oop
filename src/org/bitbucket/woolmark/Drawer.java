package org.bitbucket.woolmark;

public interface Drawer {

    void setColor(int rgb);

    void setColor(int r, int g, int b);

    void fillRect(int x, int y, int width, int height);

    void setFontSize(final int size);

    void drawText(final String text, int x, int y);

    void drawImage(final java.lang.Object image, int x, int y);

}
