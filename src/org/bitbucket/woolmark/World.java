package org.bitbucket.woolmark;

import java.util.List;

public interface World extends Object {

    <T extends Object> T newObject(final Class<T> clazz);

    void insertObject(final int index, final Object object);

    void addObject(final Object object);

    void removeObject(final Object object);

    <T extends Object> T findFirstObject(final Class<T> clazz);

    List<Object> getObjects();

    void dispose();

    void sleep() throws InterruptedException;

    void onEvent(final Event event);

    void dispatchEvent(Event event);

}
