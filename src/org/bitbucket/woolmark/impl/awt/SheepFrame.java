package org.bitbucket.woolmark.impl.awt;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.bitbucket.woolmark.SheepWorld;

public class SheepFrame extends Frame {

    private static final long serialVersionUID = -4836891575295458725L;

    public SheepFrame() {
        super();

        SheepCanvas canvas = new SheepCanvas();
        add(canvas);
        pack();

        setTitle(SheepWorld.TITLE);
        setResizable(false);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

}
