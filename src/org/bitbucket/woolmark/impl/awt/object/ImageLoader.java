package org.bitbucket.woolmark.impl.awt.object;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class ImageLoader {

    private static final String RESOURCE_DIR = "res";

    public static Image loadImage(final String fileName) {

        Image image = null;
        InputStream is = null;
        try {
            is = AwtFence.class.getResourceAsStream("/" + fileName);
            if (is == null) {
                is = new FileInputStream(new File(RESOURCE_DIR, fileName));
            }

            image = ImageIO.read(is);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }

        return image;

    }

}
