package org.bitbucket.woolmark.impl.awt.object;

import java.awt.Image;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.object.AbstractFence;
import org.bitbucket.woolmark.object.Fence;

public class AwtFence extends AbstractFence<Image> implements Fence {

    public static final String FENCE_IMAGE_NAME = "fence.png";

    private static Image image;

    static {
        image = ImageLoader.loadImage(FENCE_IMAGE_NAME);
    }

    public AwtFence(final World world) {
        super(world);
    }

    @Override
    public int getWidth() {
        return getImage().getWidth(null);
    }

    @Override
    public int getHeight() {
        return getImage().getHeight(null);
    }

    public Image getImage() {
        return image;
    }

}
