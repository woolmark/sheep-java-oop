package org.bitbucket.woolmark.impl.awt.object;

import java.awt.Image;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.object.AbstractSheep;
import org.bitbucket.woolmark.object.Sheep;

public class AwtSheep extends AbstractSheep<Image> implements Sheep {

    public static final String SHEEP_IMAGE_NAME00 = "sheep00.png";

    public static final String SHEEP_IMAGE_NAME01 = "sheep01.png";

    private static Image[] images = new Image[2];

    static {
        images[0] = ImageLoader.loadImage(SHEEP_IMAGE_NAME00);
        images[1] = ImageLoader.loadImage(SHEEP_IMAGE_NAME01);
    }

    public AwtSheep(final World world) {
        super(world);
    }

    @Override
    public int getWidth() {
        return images[0].getWidth(null);
    }

    @Override
    public int getHeight() {
        return images[0].getHeight(null);
    }

    @Override
    public Image getImage() {

        Image image;

        if (isStrech()) {
            image = images[0];
        } else {
            image = images[1];
        }

        return image;
    }

}
