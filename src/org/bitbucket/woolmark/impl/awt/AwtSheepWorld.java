package org.bitbucket.woolmark.impl.awt;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.awt.object.AwtFence;
import org.bitbucket.woolmark.impl.awt.object.AwtSheep;
import org.bitbucket.woolmark.impl.basic.AbstractSheepWorld;
import org.bitbucket.woolmark.object.Fence;
import org.bitbucket.woolmark.object.Sheep;

public class AwtSheepWorld extends AbstractSheepWorld {

    @Override
    protected Fence createFence(World world) {
        return world.newObject(AwtFence.class);
    }

    @Override
    protected Sheep createSheep(World world) {
        return world.newObject(AwtSheep.class);
    }

}
