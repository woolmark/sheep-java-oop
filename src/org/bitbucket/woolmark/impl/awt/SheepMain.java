package org.bitbucket.woolmark.impl.awt;

import java.awt.Frame;

public class SheepMain {

    public static void main(String... args) {

        Frame window = new SheepFrame();
        window.setVisible(true);
        window.toFront();

    }

}
