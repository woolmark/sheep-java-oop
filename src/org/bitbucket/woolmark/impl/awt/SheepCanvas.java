package org.bitbucket.woolmark.impl.awt;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.SheepEvent;

/**
 * Sheep Canvas.
 *
 * @author takkie
 */
public class SheepCanvas extends Canvas implements Runnable {

    private static final long serialVersionUID = 6996930354280878412L;

    private World world = null;

    private AwtDrawer drawer = null;

    public SheepCanvas() {
        world = createWorld();
        drawer = (AwtDrawer) createWorldDrawer();

        setSize(world.getWidth(), world.getHeight());

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                world.dispatchEvent(new SheepEvent(SheepEvent.START_APPEND_SHEEP));
            }

            @Override
            public void mouseReleased(MouseEvent event) {
                world.dispatchEvent(new SheepEvent(SheepEvent.STOP_APPEND_SHEEP));
            }
        });

        ExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.execute(this);
    }

    @Override
    public void paint(final Graphics g) {
        drawer.setGraphics(g);
        world.draw(drawer);
    }

    @Override
    public void run() {

        while (world.isAlive()) {

            world.ticktack();
            repaint();

            try {
                world.sleep();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    protected World createWorld() {
        return new AwtSheepWorld();
    }

    protected Drawer createWorldDrawer() {
        return new AwtDrawer();
    }

}
