package org.bitbucket.woolmark.impl.awt;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import org.bitbucket.woolmark.Drawer;

public class AwtDrawer implements Drawer {

    private Graphics graphics;

    public AwtDrawer() {
    }

    public AwtDrawer(final Graphics g) {
        graphics = g;
    }

    @Override
    public void setFontSize(final int size) {
        graphics.setFont(new Font(null, Font.PLAIN, size));
    }

    @Override
    public void setColor(int rgb) {
        graphics.setColor(new Color(rgb));
    }

    @Override
    public void setColor(final int r, final int g, final int b) {
        graphics.setColor(new Color(r, g, b));
    }

    @Override
    public void fillRect(int x, int y, int width, int height) {
        graphics.fillRect(x, y, width, height);
    }

    @Override
    public void drawText(String text, int x, int y) {
        graphics.drawString(text, x, y + graphics.getFont().getSize());
    }

    @Override
    public void drawImage(final java.lang.Object image, int x, int y) {
        if (!(image instanceof Image)) {
            throw new IllegalArgumentException();
        }

        Image img = (Image) image;

        graphics.drawImage(img, x, y, null);

    }

    public void setGraphics(final Graphics g) {
        graphics = g;
    }

}
