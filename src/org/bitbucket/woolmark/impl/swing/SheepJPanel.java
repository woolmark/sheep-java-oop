package org.bitbucket.woolmark.impl.swing;

import java.awt.Graphics;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JPanel;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.awt.AwtDrawer;
import org.bitbucket.woolmark.impl.awt.AwtSheepWorld;

/**
 * Sheep Canvas.
 *
 * @author takkie
 */
public class SheepJPanel extends JPanel implements Runnable {

    private static final long serialVersionUID = 361953028642186396L;

    private World world = null;

    private AwtDrawer drawer = null;

    public SheepJPanel() {
        world = createWorld();
        drawer = (AwtDrawer) createWorldDrawer();
        
        setSize(world.getWidth(), world.getHeight());

        ExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.execute(this);
    }

    @Override
    public void paintComponent(final Graphics g) {
        drawer.setGraphics(g);
        world.draw(drawer);
    }

    @Override
    public void run() {

        while (world.isAlive()) {

            world.ticktack();
            repaint();

            try {
                world.sleep();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    protected World createWorld() {
        return new AwtSheepWorld();
    }

    protected Drawer createWorldDrawer() {
        return new AwtDrawer();
    }

}
