package org.bitbucket.woolmark.impl.swing;

import javax.swing.JFrame;
import javax.swing.JWindow;

public class SheepMain {

    public static void main(String... args) {

        JWindow window = new SheepJWindow(new JFrame());
        window.setVisible(true);
        window.toFront();

    }

}
