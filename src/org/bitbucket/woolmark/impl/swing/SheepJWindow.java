package org.bitbucket.woolmark.impl.swing;

import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

import org.bitbucket.woolmark.impl.awt.SheepCanvas;

public class SheepJWindow extends JWindow {

    private static final long serialVersionUID = -1218019336886248653L;

    private SheepCanvas canvas;

    public SheepJWindow(JFrame frame) {

        super(frame);

        canvas = new SheepCanvas();

        DragWindowListener dwl = new DragWindowListener();
        canvas.addMouseListener(dwl);
        canvas.addMouseMotionListener(dwl);

        add(canvas);
        pack();

    }

    private class DragWindowListener extends MouseAdapter {
        private MouseEvent start;
        private Window window;

        @Override
        public void mousePressed(MouseEvent me) {
            start = me;
        }

        @Override
        public void mouseDragged(MouseEvent me) {
            if (window == null) {
                window = SwingUtilities.windowForComponent(me.getComponent());
            }
            Point eventLocationOnScreen = me.getLocationOnScreen();
            window.setLocation(eventLocationOnScreen.x - start.getX(),
                    eventLocationOnScreen.y - start.getY());
        }
    }

}
