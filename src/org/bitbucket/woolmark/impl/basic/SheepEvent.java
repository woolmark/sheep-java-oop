package org.bitbucket.woolmark.impl.basic;


public class SheepEvent extends BasicEvent {

    public static final int START_APPEND_SHEEP = 0x1;

    public static final int STOP_APPEND_SHEEP = 0x2;

    public SheepEvent() {
    }

    public SheepEvent(final int type) {
        setType(type);
    }

}
