package org.bitbucket.woolmark.impl.basic;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.Object;
import org.bitbucket.woolmark.World;

public abstract class AbstractObject implements Object {

    private int x;

    private int y;

    private int width;

    private int height;

    private boolean disposed;

    private World world;

    public AbstractObject(final World world) {
        this.world = world;

        x = 0;
        y = 0;
        width = 0;
        height = 0;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void move(int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }

    @Override
    public void setPosition(final int x, final int y) {
        setX(x);
        setY(y);
    }

    @Override
    public void setX(final int x) {
        this.x = x;
    }

    @Override
    public void setY(final int y) {
        this.y = y;
    }

    @Override
    public void setWidth(final int width) {
        this.width = width;
    }

    @Override
    public void setHeight(final int height) {
        this.height = height;
    }

    @Override
    public void setSize(final int width, final int height) {
        setWidth(width);
        setHeight(height);
    }

    @Override
    public void ticktack() {
    }

    @Override
    public void draw(Drawer drawer) {
    }

    @Override
    public boolean isAlive() {
        return !disposed;
    }

    @Override
    public void alive(boolean alive) {
        disposed = !disposed;
    }

    public World getWorld() {
        return world;
    }

    void setWorld(final World world) {
        this.world = world;
    }

}
