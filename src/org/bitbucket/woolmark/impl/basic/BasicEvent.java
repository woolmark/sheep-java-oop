package org.bitbucket.woolmark.impl.basic;

import org.bitbucket.woolmark.Event;

public class BasicEvent implements Event {

    private int type;

    public BasicEvent() {
    }

    public BasicEvent(final int type) {
        setType(type);
    }

    public void setType(final int type) {
        this.type = type;
    }

    @Override
    public int getType() {
        return type;
    }

}
