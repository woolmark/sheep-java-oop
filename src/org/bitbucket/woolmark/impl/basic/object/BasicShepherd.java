package org.bitbucket.woolmark.impl.basic.object;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.AbstractObject;
import org.bitbucket.woolmark.object.Sheep;
import org.bitbucket.woolmark.object.Shepherd;

public class BasicShepherd extends AbstractObject implements Shepherd {

    private int count;

    public BasicShepherd(final World world) {
        super(world);
    }

    @Override
    public void ticktack() {

        for (Object object : getWorld().getObjects()) {
            if (Sheep.class.isInstance(object)) {
                Sheep sheep = (Sheep) object;

                if (sheep.getJumpFrame() == Sheep.JUMP_FRAME) {
                    count++;
                }
            }
        }

    }

    @Override
    public void draw(final Drawer drawer) {
        drawer.setColor(MESSAGE_COLOR);
        drawer.drawText(String.format(MESSAGE_FORMAT, getSheepCount()), MESSAGE_X, MESSAGE_Y);
    }

    @Override
    public int getSheepCount() {
        return count;
    }

}
