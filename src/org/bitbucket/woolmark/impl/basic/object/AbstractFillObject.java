package org.bitbucket.woolmark.impl.basic.object;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.AbstractObject;

public abstract class AbstractFillObject extends AbstractObject {

    public AbstractFillObject(final World world) {
        super(world);
    }

    @Override
    public void draw(final Drawer drawer) {
        drawer.setColor(getFillColorRed(), getFillColorGreen(), getFillColorBlue());
        drawer.fillRect(getX(), getY(), getWidth(), getHeight());
    }

    protected abstract int getFillColorRed();

    protected abstract int getFillColorGreen();

    protected abstract int getFillColorBlue();

}
