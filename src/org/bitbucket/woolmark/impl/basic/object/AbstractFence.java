package org.bitbucket.woolmark.impl.basic.object;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.object.Fence;

public abstract class AbstractFence<I> extends AbstractImageObject<I> implements Fence {

    public AbstractFence(World world) {
        super(world);
        setPosition((world.getWidth() - getWidth()) / 2, world.getHeight() - getHeight());
    }

}
