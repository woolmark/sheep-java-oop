package org.bitbucket.woolmark.impl.basic.object;

import java.util.Random;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.object.Fence;
import org.bitbucket.woolmark.object.Ground;
import org.bitbucket.woolmark.object.Sheep;

public abstract class AbstractSheep<I> extends AbstractImageObject<I> implements Sheep {

    private static final Random random = new Random();

    private int jumpX;

    private int jumpFrame = Integer.MIN_VALUE;

    private boolean strech;

    public AbstractSheep(World world) {
        super(world);

        Ground ground = world.findFirstObject(Ground.class);
        if (ground != null) {
            setPosition(ground.getWidth() + getWidth(),
                    world.getHeight() - ground.getHeight()
                            + random.nextInt(ground.getHeight() - getHeight()));
        }

        jumpX = calcJumpX();
    }

    @Override
    public void ticktack() {

        if (isStartJump()) {
            jumpFrame = 0;
        }

        int moveY = 0;
        if (jumpFrame >= 0) {

            if (jumpFrame < JUMP_FRAME) {
                moveY = -1 * DELTA_Y;
            } else if (jumpFrame < JUMP_FRAME * 2) {
                moveY = DELTA_Y;
            } else {
                jumpFrame = Integer.MIN_VALUE;
            }

            jumpFrame++;
        }

        move(-1 * DELTA_X, moveY);

        if (getX() < -1 * getWidth()) {
            alive(false);
        }

        strech = !strech;
    }

    public int getJumpFrame() {
        return jumpFrame;
    }

    protected boolean isStrech() {

        boolean strech = this.strech;
        if (jumpFrame >= 0) {
            strech = true;
        }

        return strech;
    }

    private boolean isStartJump() {

        boolean startJump = false;

        if (jumpFrame < 0 && jumpX <= getX() && getX() < jumpX + DELTA_X) {
            startJump = true;
        }

        return startJump;
    }

    private int calcJumpX() {
        // y = -1 * fence.height / fence.width * (x - (frame.width - fence.width) / 2) + frame.height

        World world = getWorld();
        int fw = world.getWidth();
        int fh = world.getHeight();

        Fence fence = world.findFirstObject(Fence.class);
        int w = fence.getWidth();
        int h = fence.getHeight();

        return -1 * (getY() - fh) * w / h + (fw - w) / 2;
    }

}
