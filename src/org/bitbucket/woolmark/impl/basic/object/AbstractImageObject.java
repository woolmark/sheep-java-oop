package org.bitbucket.woolmark.impl.basic.object;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.AbstractObject;

public abstract class AbstractImageObject<I> extends AbstractObject {

    public AbstractImageObject(World world) {
        super(world);
    }

    @Override
    public void draw(final Drawer drawer) {
        drawer.drawImage(getImage(), getX(), getY());
    }

    public abstract I getImage();

}
