package org.bitbucket.woolmark.impl.basic.object;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.object.Fence;
import org.bitbucket.woolmark.object.Ground;

public class BasicGround extends AbstractFillObject implements Ground {

    public BasicGround(final World world) {
        super(world);
        setSize(world.getWidth(), world.getHeight());
    }

    public void setSize(final Fence fence) {
        setHeight((int) (fence.getHeight() * 0.9f));
        setY(getWorld().getHeight() - getHeight());
    }

    @Override
    protected int getFillColorRed() {
        return FILL_COLOR_RED;
    }

    @Override
    protected int getFillColorGreen() {
        return FILL_COLOR_GREEN;
    }

    @Override
    protected int getFillColorBlue() {
        return FILL_COLOR_BLUE;
    }

}
