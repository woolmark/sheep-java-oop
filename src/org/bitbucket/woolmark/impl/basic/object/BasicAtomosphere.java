package org.bitbucket.woolmark.impl.basic.object;

import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.object.Atmosphere;

public class BasicAtomosphere extends AbstractFillObject implements Atmosphere {

    public BasicAtomosphere(final World world) {
        super(world);
        setSize(world.getWidth(), world.getHeight());
    }

    @Override
    protected int getFillColorRed() {
        return FILL_COLOR_RED;
    }

    @Override
    protected int getFillColorGreen() {
        return FILL_COLOR_GREEN;
    }

    @Override
    protected int getFillColorBlue() {
        return FILL_COLOR_BLUE;
    }

}
