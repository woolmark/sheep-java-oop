package org.bitbucket.woolmark.impl.basic;

import org.bitbucket.woolmark.Event;
import org.bitbucket.woolmark.SheepWorld;
import org.bitbucket.woolmark.World;
import org.bitbucket.woolmark.impl.basic.object.BasicAtomosphere;
import org.bitbucket.woolmark.impl.basic.object.BasicGround;
import org.bitbucket.woolmark.impl.basic.object.BasicShepherd;
import org.bitbucket.woolmark.object.Atmosphere;
import org.bitbucket.woolmark.object.Fence;
import org.bitbucket.woolmark.object.Ground;
import org.bitbucket.woolmark.object.Sheep;
import org.bitbucket.woolmark.object.Shepherd;

public abstract class AbstractSheepWorld extends AbstractWorld implements SheepWorld {

    private boolean appendSheep;

    public AbstractSheepWorld() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public AbstractSheepWorld(final int width, final int height) {
        super(width, height);

        Atmosphere atmosphere = createAtmosphere(this);

        Fence fence = createFence(this);

        Ground ground = createGround(this);
        ground.setSize(fence);

        addObject(atmosphere);
        addObject(ground);
        addObject(fence);

        Sheep sheep = createSheep(this);
        addObject(sheep);

        Shepherd shepherd = createShepherd(this);
        addObject(shepherd);
    }

    @Override
    public void ticktack() {
        if (appendSheep) {
            Sheep sheep = createSheep(this);
            addObject(sheep);
        }

        super.ticktack();

        if (findFirstObject(Sheep.class) == null) {
            addObject(createSheep(this));
        }
    }

    public void onEvent(final Event event) {
        
        switch (event.getType()) {
        case SheepEvent.START_APPEND_SHEEP:
            appendSheep = true;
            break;
        case SheepEvent.STOP_APPEND_SHEEP:
            appendSheep = false;
            break;
        default:
            break;
        }
    }

    protected abstract Fence createFence(final World world);

    protected abstract Sheep createSheep(final World world);

    protected Atmosphere createAtmosphere(final World world) {
        return world.newObject(BasicAtomosphere.class);
    }

    protected Ground createGround(final World world) {
        return world.newObject(BasicGround.class);
    }

    protected Shepherd createShepherd(final World world) {
        return world.newObject(BasicShepherd.class);
    }

}
