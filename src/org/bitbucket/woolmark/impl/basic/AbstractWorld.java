package org.bitbucket.woolmark.impl.basic;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bitbucket.woolmark.Drawer;
import org.bitbucket.woolmark.Event;
import org.bitbucket.woolmark.Object;
import org.bitbucket.woolmark.World;

public abstract class AbstractWorld extends AbstractObject implements World {

    public static final int DEFAULT_INTERVAL = 100;

    private long lastSleepTime = System.currentTimeMillis();

    private List<Object> objects = new ArrayList<Object>();

    private List<Event> events = new ArrayList<Event>();

    public AbstractWorld(int width, int height) {
        super(null);
        setSize(width, height);
    }

    public <T extends Object> T newObject(final Class<T> clazz) {

        T object = null;
        try {
            object = clazz.getConstructor(World.class).newInstance(this);

            if (object instanceof AbstractObject) {
                ((AbstractObject) object).setWorld(this);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return object;
    }

    @Override
    public void insertObject(int index, Object object) {
        if (!isAlive()) {
            throw new IllegalStateException("world is disposed.");
        }

        if (object.getWorld() != this) {
            throw new IllegalArgumentException("object is not for this world.");
        }

        objects.add(index, object);
    }

    @Override
    public void addObject(Object object) {
        if (!isAlive()) {
            throw new IllegalStateException("world is disposed.");
        }

        if (object == null) {
            throw new NullPointerException("object is null.");
        }

        if (object.getWorld() != this) {
            throw new IllegalArgumentException("object is not for this world.");
        }

        objects.add(object);
    }

    @Override
    public void removeObject(Object object) {
        if (!isAlive()) {
            throw new IllegalStateException("world is disposed.");
        }

        if (object.getWorld() != this) {
            throw new IllegalArgumentException("object is not for this world.");
        }

        objects.remove(object);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Object> T findFirstObject(final Class<T> clazz) {

        T obj = null;

        for (Object object : objects) {
            if (clazz.isInstance(object)) {
                obj = (T) object;
                break;
            }
        }

        return obj;
    }

    @Override
    public List<Object> getObjects() {
        if (!isAlive()) {
            throw new IllegalStateException("world is disposed.");
        }

        return Collections.unmodifiableList(objects);
    }

    @Override
    public synchronized void ticktack() {

        for (Event event : events) {
            onEvent(event);
        }

        List<Object> deadObjects = new ArrayList<Object>();

        for (Object object : objects) {
            object.ticktack();

            if (!object.isAlive()) {
                deadObjects.add(object);
            }
        }

        objects.removeAll(deadObjects);
        deadObjects.clear();

    }

    @Override
    public synchronized void draw(Drawer drawer) {
        for (Object object : getObjects()) {
            object.draw(drawer);
        }
    }

    @Override
    public void sleep() throws InterruptedException {

        long intervalDiff = System.currentTimeMillis() - lastSleepTime;

        if (intervalDiff >= 0 && intervalDiff < getInterval()) {
            Thread.sleep(getInterval() - intervalDiff);
        }

        lastSleepTime = System.currentTimeMillis();
    }

    public void dispatchEvent(final Event event) {
        events.add(event);
    }

    @Override
    public void dispose() {
    }

    protected long getInterval() {
        return DEFAULT_INTERVAL;
    }

}
