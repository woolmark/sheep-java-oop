package org.bitbucket.woolmark;

public interface SheepWorld extends World {

    public static final int DEFAULT_WIDTH = 120;

    public static final int DEFAULT_HEIGHT = 120;

    public static final String TITLE = "sheep";

}
