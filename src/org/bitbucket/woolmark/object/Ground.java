package org.bitbucket.woolmark.object;

import org.bitbucket.woolmark.Object;

public interface Ground extends Object {

    public static final int FILL_COLOR_RED = 100;

    public static final int FILL_COLOR_GREEN = 255;

    public static final int FILL_COLOR_BLUE = 100;

    public void setSize(final Fence fence);

}
