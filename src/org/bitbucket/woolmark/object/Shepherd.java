package org.bitbucket.woolmark.object;

import org.bitbucket.woolmark.Object;

public interface Shepherd extends Object {

    String MESSAGE_FORMAT = "羊が%d匹";

    int MESSAGE_X = 5;

    int MESSAGE_Y = 5;

    int MESSAGE_COLOR = 0x000000;

    int getSheepCount();

}
