package org.bitbucket.woolmark.object;

import org.bitbucket.woolmark.Object;

public interface Sheep extends Object {

    int DELTA_X = 5;

    int DELTA_Y = 3;

    int JUMP_FRAME = 4;

    int getJumpFrame();

}
