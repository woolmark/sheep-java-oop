package org.bitbucket.woolmark.object;

import org.bitbucket.woolmark.Object;

public interface Atmosphere extends Object {

    public static final int FILL_COLOR_RED = 150;

    public static final int FILL_COLOR_GREEN = 150;

    public static final int FILL_COLOR_BLUE = 255;

}
