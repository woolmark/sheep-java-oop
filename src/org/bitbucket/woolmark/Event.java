package org.bitbucket.woolmark;

public interface Event {

    public int getType();

}
